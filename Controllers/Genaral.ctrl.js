// import {AdminService} from '../Models/services/index';
// import Util from '../Utils/Util';
// import Congif from '../Utils/config';
// import bcrypt from 'bcryptjs';
// import { validationResult } from 'express-validator';
// import jwt from 'jsonwebtoken';
// import filterError from '../Utils/filterError';

// const util = new Util();

// class ctrlTaiKhoan {
	
// 	/**
// 	 *  general
// 	 */
// 	static async login(req, res) {
// 		try {
// 			const validate = validationResult(req);
// 			if (!validate.isEmpty()) {
// 				util.setError(400, validate.array());
// 				return util.send(res);
// 			}
// 			let { email, password, type } = req.body;
// 			const user = await serviceTaiKhoan.getByEmailAndType(email, type);
// 			if (!user) {
// 				util.setError(400, [{ msg: 'Tài khoản không tồn tại hoặc không có quyền truy cập' }]);
// 				return util.send(res);
// 			}
// 			let checkPassword = bcrypt.compareSync(password, user.password);
// 			if (checkPassword) {
// 				let token = jwt.sign({ userID: user.id, role: user.role }, Congif.token.secret, {
// 					expiresIn: Congif.token.life,
// 				});

// 				util.setSuccess(200, {
// 					token,
// 				});
// 				return util.send(res);
// 			} else {
// 				util.setError(400, [{ msg: 'Mật khẩu không chính xác' }]);
// 				return util.send(res);
// 			}
// 		} catch (err) {
// 			let message = filterError(err.errors);
// 			util.setError(500, message);
// 			return util.send(res);
// 		}
// 	}

//   static async loginAdmin(req,res){
//     const validate = validationResult(req);
//     if (!validate.isEmpty()) {
//       util.setError(400, validate.array());
//       return util.send(res);
//     }
//     let { email, password } = req.body;
//     const user = await AdminService.getByEmail(email);
//     if (!user) {
//       util.setError(400, [{ msg: 'Tài khoản không tồn tại' }]);
//       return util.send(res);
//     }
//     let checkPassword = bcrypt.compareSync(password, user.password);
//     if (checkPassword) {
//       let token = jwt.sign({ userID: user.id, role: user.role }, Congif.token.secret, {
//         expiresIn: Congif.token.life,
//       });

//       util.setSuccess(200, {
//         token,
//       });
//       return util.send(res);
//     } else {
//       util.setError(400, [{ msg: 'Mật khẩu không chính xác' }]);
//       return util.send(res);
//     }
//   }
// 	// static logout(req, res) {}

// 	static async fetchMe(req, res) {
// 		try {
// 			let user = await serviceTaiKhoan.getOne(req.userID);
// 			if (!user) {
// 				util.setError(400, [{ msg: 'Không tìm thấy tài khoản' }]);
// 				return util.send(res);
// 			}
// 			util.setSuccess(200, user);
// 			return util.send(res);
// 		} catch (err) {
// 			let message = filterError(err.errors);
// 			util.setError(500, message);
// 			return util.send(res);
// 		}
// 	}

// 	static async updateTK(req, res) {
// 		const validate = validationResult(req);
// 		if (!validate.isEmpty()) {
// 			util.setError(400, validate.array());
// 			return util.send(res);
// 		}
// 		let data = req.body;
// 		try {
// 			let updatedTK = await serviceTaiKhoan.updateTK(req.params.id, data);
// 			util.setSuccess(200, { isUpdated: updatedTK });
// 			return util.send(res);
// 		} catch (error) {
// 			util.setError(400, error);
// 			return util.send(res);
// 		}
// 	}
// 	static async changePassword(req, res) {
// 		const validate = validationResult(req);
// 		if (!validate.isEmpty()) {
// 			util.setError(400, validate.array());
// 			return util.send(res);
// 		}
// 		let { password, newPassword } = req.body;
// 		if (password === newPassword) {
// 			util.setError(400, [{ msg: 'Không được nhập giống mật khẩu cũ' }]);
// 			return util.send(res);
// 		}
// 		try {
// 			let user = await serviceTaiKhoan.getOne(req.req.params.id);
// 			let checkPassword = bcrypt.compareSync(password, user.password);
// 			if (checkPassword) {
// 				let updatedPassword = await serviceTaiKhoan.updateTK(req.req.params.id, {
// 					password: bcrypt.hashSync(newPassword, Congif.saltRounds),
// 				});
// 				util.setSuccess(200, { isUpdated: updatedPassword });
// 				return util.send(res);
// 			} else {
// 				util.setError(400, [{ msg: 'Mật khẩu không chính xác' }]);
// 				return util.send(res);
// 			}
// 		} catch (err) {
// 			let message = filterError(err.errors);
// 			util.setError(500, message);
// 			return util.send(res);
// 		}
// 	}
// 	// ########### end general

// 	/**
// 	 *  student
// 	 */

// 	static async registerStudent(req, res) {
// 		try {
// 			const validate = validationResult(req);
// 			if (!validate.isEmpty()) {
// 				util.setError(400, validate.array());
// 				return util.send(res);
// 			}
// 			let data = {
// 				...req.body,
// 				role: 'student',
// 				password: bcrypt.hashSync(req.body.password, Congif.saltRounds),
// 			};
// 			let createdTK = await serviceTaiKhoan.addTK(data);
// 			util.setSuccess(201, { isCreated: createdTK });
// 			return util.send(res);
// 		} catch (err) {
// 			let message = filterError(err.errors);
// 			util.setError(500, message);
// 			return util.send(res);
// 		}
// 	}

// 	// ########## end student
// }
// export default ctrlTaiKhoan;
