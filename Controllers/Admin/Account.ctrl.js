import bcrypt from 'bcryptjs';
import { validationResult } from 'express-validator';
import jwt from 'jsonwebtoken';

// util
import Util from '../../Utils/Util';
import Congif from '../../Utils/config';

import { AdminService, StudentService, TeacherService } from '../../Models/services/index';

const util = new Util();

export default class AccounCtrl {
	/**
	 * get list account
	 */
	static async getListAdmin(req, res) {
		const { page, limit } = req.query;
		let offset = Number(page) * Number(limit) - Number(limit);
		const listTK = await AdminService.getListAdmin(limit, offset);
		if (listTK.length > 0) {
			util.setSuccess(200, { docs: listTK, page, limit });
		} else {
			util.setSuccess(200, { docs: [], page, limit });
		}
		return util.send(res);
	}

	// create account admin
	static async createAdmin(req, res) {
		const validate = validationResult(req);
		if (!validate.isEmpty()) {
			util.setError(400, validate.array());
			return util.send(res);
		}
		let data = {
			...req.body,
			password: bcrypt.hashSync(req.body.password, Congif.saltRounds),
		};
		await AdminService.createAdmin(data);
		util.setSuccess(201, { data });
		return util.send(res);
	}

	/**
	 * update account admin
	 */
	static async updateAdmin(req, res) {
		const { ID } = req.params;
		if (req.body.role) {
			util.setError(400, [{ msg: 'Không thể cập nhật loại tài khoản !' }]);
			return util.send(res);
		}
		if (req.body.password) {
			util.setError(400, [{ msg: 'Không thể cập nhật mật khẩu !' }]);
			return util.send(res);
		}
		await AdminService.updateAdmin(ID, req.body, req.role);
		util.setSuccess(200, { isUpdate: true });
		return util.send(res);
	}
	// change password admin
	static async changePasswordAdmin(req, res) {
		const { ID } = req.params;
		const { password, newPassword } = req.body;
		const validate = validationResult(req);
		if (!validate.isEmpty()) {
			util.setError(400, validate.array());
			return util.send(res);
		}
		let userPassword = await AdminService.getPassword(ID);
		let checkPassword = bcrypt.compareSync(password, userPassword.password);
		if (checkPassword) {
			await AdminService.updateAdmin(ID, {
				password: bcrypt.hashSync(newPassword, Congif.saltRounds),
			});
			util.setSuccess(200, { isUpdated: true });
			return util.send(res);
		} else {
			util.setError(400, [{ msg: 'Mật khẩu không chính xác' }]);
			return util.send(res);
		}
	}
	/**
	 * delete account admin
	 */
	static async deleteAdmin(req, res) {
		let result = await AdminService.deleteAdmin(req.params.ID);
		if (result.status === 'success') util.setSuccess(200, { isDeleted: true });
		else util.setError(400, [{ msg: result.msg }]);
		return util.send(res);
	}

	/**
	 * account student
	 */

	// get list student

	static async getListStudent(req, res) {
		const { page, limit } = req.query;
		let offset = Number(page) * Number(limit) - Number(limit);
		const listTK = await StudentService.getListStudent(limit, offset);
		if (listTK.length > 0) {
			util.setSuccess(200, { docs: listTK, page, limit, total: listTK.length });
		} else {
			util.setSuccess(200, { docs: [], page, limit });
		}
		return util.send(res);
	}

	// create account student
	static async createStudent(req, res) {
		const validate = validationResult(req);
		if (!validate.isEmpty()) {
			util.setError(400, validate.array());
			return util.send(res);
		}
		let data = {
			...req.body,
			password: bcrypt.hashSync(req.body.password, Congif.saltRounds),
		};
		await StudentService.createStudent(data);
		util.setSuccess(201, { data });
		return util.send(res);
	}

	// update account student
	static async updateStudent(req, res) {
		const { ID } = req.params;
		if (req.body.role) {
			util.setError(400, [{ msg: 'Không thể cập nhật loại tài khoản !' }]);
			return util.send(res);
		}
		if (req.body.password) {
			util.setError(400, [{ msg: 'Không thể cập nhật mật khẩu !' }]);
			return util.send(res);
		}
		await StudentService.updateStudent(ID, req.body);
		util.setSuccess(200, { isUpdate: true });
		return util.send(res);
	}

	// change password student
	static async changePasswordStudent(req, res) {
		const { ID } = req.params;
		const { password, newPassword } = req.body;
		const validate = validationResult(req);
		if (!validate.isEmpty()) {
			util.setError(400, validate.array());
			return util.send(res);
		}
		let userPassword = await StudentService.getPassword(ID);
		let checkPassword = bcrypt.compareSync(password, userPassword.password);
		if (checkPassword) {
			await StudentService.updateStudent(ID, {
				password: bcrypt.hashSync(newPassword, Congif.saltRounds),
			});
			util.setSuccess(200, { isUpdated: true });
			return util.send(res);
		} else {
			util.setError(400, [{ msg: 'Mật khẩu không chính xác' }]);
			return util.send(res);
		}
	}
	// delete account student
	static async deleteStudent(req, res) {
		await StudentService.deleteStudent(req.params.ID);
		util.setSuccess(200, { isDeleted: true });
		return util.send(res);
	}

	/**
	 *  teacher
	 */

	// get list teacher

	static async getListTeacher(req, res) {
		const { page, limit } = req.query;
		let offset = Number(page) * Number(limit) - Number(limit);
		const listTK = await TeacherService.getList(limit, offset);
		if (listTK.length > 0) {
			util.setSuccess(200, { docs: listTK, page, limit, total: listTK.length });
		} else {
			util.setSuccess(200, { docs: [], page, limit });
		}
		return util.send(res);
	}

	// create teacher

	static async createTeacher(req, res) {
		const validate = validationResult(req);
		if (!validate.isEmpty()) {
			util.setError(400, validate.array());
			return util.send(res);
		}
		let data = {
			...req.body,
			password: bcrypt.hashSync(req.body.password, Congif.saltRounds),
		};
		await TeacherService.createTeacher(data);
		util.setSuccess(201, { data });
		return util.send(res);
	}

	// update teacher

	static async updateTeacher(req, res) {
		const { ID } = req.params;
		if (req.body.role) {
			util.setError(400, [{ msg: 'Không thể cập nhật loại tài khoản !' }]);
			return util.send(res);
		}
		if (req.body.password) {
			util.setError(400, [{ msg: 'Không thể cập nhật mật khẩu !' }]);
			return util.send(res);
		}
		await TeacherService.updateTeacher(ID, req.body);
		util.setSuccess(200, { isUpdate: true });
		return util.send(res);
	}

	// change password teacher
	static async changePasswordTeacher(req, res) {
		const { ID } = req.params;
		const { password, newPassword } = req.body;
		const validate = validationResult(req);
		if (!validate.isEmpty()) {
			util.setError(400, validate.array());
			return util.send(res);
		}
		let userPassword = await TeacherService.getPassword(ID);
		let checkPassword = bcrypt.compareSync(password, userPassword.password);
		if (checkPassword) {
			await TeacherService.updateTeacher(ID, {
				password: bcrypt.hashSync(newPassword, Congif.saltRounds),
			});
			util.setSuccess(200, { isUpdated: true });
			return util.send(res);
		} else {
			util.setError(400, [{ msg: 'Mật khẩu không chính xác' }]);
			return util.send(res);
		}
	}

	// delete teacher

	static async deleteTeacher(req, res) {
		await TeacherService.deleteTeacher(req.params.ID);
		util.setSuccess(200, { isDeleted: true });
		return util.send(res);
	}

	// general

	// login admin
	static async loginAdmin(req, res) {
		const validate = validationResult(req);
		if (!validate.isEmpty()) {
			util.setError(400, validate.array());
			return util.send(res);
		}
		let { email, password } = req.body;
		const user = await AdminService.getPasswordByEmail(email);
		if (!user) {
			util.setError(400, [{ msg: 'Tài khoản không tồn tại' }]);
			return util.send(res);
		}
		let checkPassword = bcrypt.compareSync(password, user.password);
		if (checkPassword) {
			if (!user.status) util.setError(403, [{ msg: 'Tài khoản của bạn đã bị cấm hoạt động' }]);
			else {
				let token = jwt.sign({ userID: user.id, role: user.role }, Congif.token.secret, {
					expiresIn: Congif.token.life,
				});
				util.setSuccess(200, {
					token,
				});
			}
			return util.send(res);
		} else {
			util.setError(400, [{ msg: 'Mật khẩu không chính xác' }]);
			return util.send(res);
		}
	}

	// get profile admin
	static async getProfileAdmin(req, res) {
		let user = await AdminService.getOne(req.userID);
		if (!user) {
			util.setError(400, [{ msg: 'Không tìm thấy tài khoản' }]);
			return util.send(res);
		}
		util.setSuccess(200, user);
		return util.send(res);
	}

	// search account student
	static async searchStudent(req, res) {
		const { limit, page, keyword } = req.query;
		let offset = Number(page) * Number(limit) - Number(limit);
		let data =await StudentService.searchStudent(limit, offset, keyword);
		if (data.length > 0) {
			util.setSuccess(200, { docs: data, page, limit });
		} else {
			util.setSuccess(200, { docs: [], page, limit });
		}
		return util.send(res);
	}
	// search account teacher
	static async searchTeacher(req, res) {
		const { limit, page, keyword } = req.query;
		let offset = Number(page) * Number(limit) - Number(limit);
		let data = await TeacherService.searchTeacher(limit, offset, keyword);
		if (data.length > 0) {
			util.setSuccess(200, { docs: data, page, limit });
		} else {
			util.setSuccess(200, { docs: [], page, limit });
		}
		return util.send(res);
	}
}
