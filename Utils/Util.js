export default class Util {
  constructor() {
    this.statusCode = null;
    this.data = null;
    this.error = null;
    this.type = null;
  }
  setSuccess(statusCode, data) {
    this.type = 'success';
    this.statusCode = statusCode;
    this.data = data;
  }
  setError(statusCode, err) {
    this.type = 'error';
    this.statusCode = statusCode;
    this.error = err;
  }
  send(res) {
    if (this.type === 'success') {
      return res.status(this.statusCode).json({
        status: this.type,
        result: this.data
      });
    }
    return res.status(this.statusCode).json({
      status: this.type,
      error: this.error
    });
  }
}
