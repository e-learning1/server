const bcrypt = require('bcryptjs');
module.exports={
  port:8000 | process.env.PORT,
  db:{
    name:'dayhoctructuyen',
    username:'postgres',
    password:'postgres'
  },
  saltRounds:bcrypt.genSaltSync(10),
  token:{
    secret:"hai-dev",
    life:1800,
  },
 
};
