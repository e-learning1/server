import jwt from 'jsonwebtoken';
import Config from './config';

/**
 * auth middleware default
 */

export const middleware = (req, res, next) => {
	var token = req.headers['x-access-token'];
	if (!token) return res.status(403).send({ status: false, error: [{ msg: 'Yêu cầu đăng nhập' }] });

	jwt.verify(token, Config.token.secret, function(err, decoded) {
		if (err) return res.status(500).send({ status: false, error: [{ msg: 'Xác thực không hợp lệ' }] });

		// if everything good, save to request for use in other routes
		req.userID = decoded.userID;
		req.role = decoded.role;
		next();
	});
};

/**
 * auth middleware for route admin
 */
export const middlewareAdmin = (req, res, next) => {
	var token = req.headers['x-access-token'];
	if (!token || typeof token === 'undefined')
		return res.status(403).send({ status: false, error: [{ msg: 'Yêu cầu đăng nhập' }] });

	jwt.verify(token, Config.token.secret, function(err, decoded) {
		if (err) return res.status(500).send({ status: false, error: [{ msg: 'Xác thực không hợp lệ' }] });

		// if token verify success
		// check role
		if (decoded.role !== 'admin' && decoded.role !== 'superAdmin')
			return res.status(403).send({ status: false, error: [{ msg: 'Tài khoản không hợp lệ' }] });
		req.userID = decoded.userID;
		req.role = decoded.role;
		next();
	});
};

/**
 * auth middleware for route teacher
 */
export const middlewareTeacher = (req, res, next) => {
	var token = req.headers['x-access-token'];
	if (!token) return res.status(403).send({ status: false, error: [{ msg: 'Yêu cầu đăng nhập' }] });

	jwt.verify(token, Config.token.secret, function(err, decoded) {
		if (err) return res.status(500).send({ status: false, error: [{ msg: 'Xác thực không hợp lệ' }] });

		// if token verify success
		// check role
		if (decoded.role !== 'teacher')
			return res.status(403).send({ status: false, error: [{ msg: 'Tài khoản không hợp lệ' }] });
		req.userID = decoded.userID;
		req.role = decoded.role;
		next();
	});
};

/**
 * auth middleware for route student
 */
export const middlewareStudent = (req, res, next) => {
	var token = req.headers['x-access-token'];
	if (!token) return res.status(403).send({ status: false, error: [{ msg: 'Yêu cầu đăng nhập' }] });

	jwt.verify(token, Config.token.secret, function(err, decoded) {
		if (err) return res.status(500).send({ status: false, error: [{ msg: 'Xác thực không hợp lệ' }] });

		// if token verify success
		// check role
		if (decoded.role !== 'student')
			return res.status(403).send({ status: false, error: [{ msg: 'Tài khoản không hợp lệ' }] });
		req.userID = decoded.userID;
		req.role = decoded.role;
		next();
	});
};
