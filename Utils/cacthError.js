import _ from 'lodash';
import Util from './Util';
const util = new Util();

/**
 * catch error in controller
 * @param {function} func
 */
export const asyncHandleErr = func => (req, res) => {
	Promise.resolve(func(req, res)).catch(err => {
		console.log(err)
		if (err.name && err.name === 'SequelizeDatabaseError') {
			util.setError(400, [{ msg: 'Lỗi dữ liệu' }]);
		}
		if (err && err.errors) {
			let message = filterError(err.errors);
			util.setError(500, message);
		} else util.setError(500, [{ msg: 'Lỗi hệ thống' }]);
		return util.send(res);
	});
};

/**
 * filter message in error
 * @param {object} err
 */
export const filterError = err => {
	let errors = [];
	if (err.length > 0) {
		errors = _.map(err, item => {
			return {
				msg: item.message,
			};
		});
	}
	return errors;
};

/**
 * create custom message
 * @param {array || string} mess message
 */
export const createMessage = mess => {
	let tempErr = { errors: [] };
	if (_.isArray(mess)) {
		tempErr.errors = _.map(mess, ele => {
			return { message: ele };
		});
	} else tempErr.errors.push({ message: mess });
	return tempErr;
};
