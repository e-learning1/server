import express from "express";
import http from "http";
import logger from "morgan";
import bodyParser from "body-parser";
import cors from 'cors';
import session from "express-session";
import config from "./Utils/config";
import routes from "./Routers/index";
import db from "./Utils/database";
/**
 * setup server
 */
const app = express();
app.use("/Resources", express.static("Resources"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(logger("dev"));
app.use(cors());
app.use(
  session({
    secret: "hai",
    cookie: { maxAge: 1000 * 60 * 3600 },
    proxy: true,
    resave: false,
    saveUninitialized: true
  })
);
app.use(routes);
/**
 * connect database
 */
db.authenticate()
  .then(() => console.log("Database connected..."))
  .catch(err => console.log(err));

/**
 *  start server
 */
let server = http.createServer(app);
server.listen(config.port, () => {
  console.log(`Server dang chay tren ${config.port}`);
});

module.exports = app;
