import app from 'express';
import { check } from 'express-validator';
import AdminAccountCtrl from '../Controllers/Admin/Account.ctrl';
import { asyncHandleErr } from '../Utils/cacthError';
const router = app.Router();

/**
 * login admin
 */
router.post(
	'/login/admin',
	[
		[
			check('email')
				.isLength({ min: 4, max: 30 })
				.withMessage('Email tối thiểu 4 ký tự & tối đa 30 ký tự')
				.isEmail()
				.withMessage('Email không hợp lệ'),
			check('password')
				.isLength({ min: 5, max: 50 })
				.withMessage('Mật khẩu tối thiểu 5 ký tự & tối đa 50 ký tự'),
		],
	],
	asyncHandleErr(AdminAccountCtrl.loginAdmin),
);

/**
 *  dang ky tai khoan hoc vien
 */
// router.post(
// 	'/student/register',
// 	[
// 		check('hoTen')
// 			.isLength({ min: 5, max: 50 })
// 			.withMessage('Tối thiểu 5 ký tự & tối đa 50 ký tự'),
// 		check('email')
// 			.isEmail()
// 			.withMessage('Email không hợp lệ'),
// 		check('khuVuc')
// 			.isLength({ min: 5, max: 50 })
// 			.withMessage('Tối thiểu 5 ký tự & tối đa 50 ký tự'),
// 		check('password')
// 			.isLength({ min: 5 })
// 			.withMessage('Tối thiểu 5 ký tự'),
// 		check('mobilePhone')
// 			.isLength({ min: 5 })
// 			.withMessage('Tối thiểu 5 ký tự'),
// 	],
// 	GenaralCtrl.registerStudent,
// );

export default router;
