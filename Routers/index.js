import app from 'express';

// import router
 // import routerStudent from './User/TaiKhoan';
import routerAdmin from './Admin/index';
import routerGeneral from './genaralRouter';
// import controller
// import GeneralCtrl from '../Controllers/Genaral.ctrl';


const router = app.Router();

/**
 *  setup routes
 */

router.use('/admin', routerAdmin);
// router.use('/student', routerStudent);
router.use(routerGeneral);

/**
 * router index
 */
// router.get(
//   '/get-profile',
//   middleware,
//   GeneralCtrl.fetchMe
// );

export default  router;
