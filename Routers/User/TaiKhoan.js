import { Router } from 'express';
import ctrlTaiKhoan from '../../Controllers/Genaral.ctrl';
import { check } from 'express-validator';
import {middleware} from '../../Utils/authRouter';

const router = Router();

/**
 * middleware router
 */
router.use(middleware);

// update profile
router.patch(
  '/account/:id',
  [
    check('hoTen')
      .not()
      .isEmpty()
      .withMessage('Không được để trống')
      .isLength({ min: 5, max: 50 })
      .withMessage('Tối thiểu 5 ký tự & tối đa 50 ký tự'),
    check('mobilePhone')
      .not()
      .isEmpty()
      .withMessage('Không được để trống')
      .isLength({ min: 11, max: 11 })
      .withMessage('Số điện thoại không hợp lệ'),
    check('khuVuc')
      .not()
      .isEmpty()
      .withMessage('Không được để trống')
  ],
  ctrlTaiKhoan.updateTK
);

// change password
router.patch(
  '/change-password/:id',
  [
    check('password')
      .not()
      .isEmpty()
      .withMessage('Không được để trống')
      .isLength({ min: 5 })
      .withMessage('Nhập tối thiểu 5 ký tự'),
    check('confirmPassword', 'Giá trị input nhập lại mật khẩu không giống giá trị input mật khẩu')
      .exists()
      .custom((value, { req }) => value === req.body.password),
    check('newPassword')
      .isEmpty()
      .withMessage('Không được để trống')
      .exists()
      .custom((value, { req }) => value === req.body.password)
      .withMessage('Không được nhập giống mật khẩu cũ')
  ],
  ctrlTaiKhoan.changePassword
);

module.exports = router;
