import { Router } from 'express';
import { check } from 'express-validator';

// import { middlewareAdmin } from '../../Utils/authRouter';
import { asyncHandleErr } from '../../Utils/cacthError';
import AccountCtrl from '../../Controllers/Admin/Account.ctrl';

const router = Router();

/**
 * middleware router
 */
// router.use(middlewareAdmin);

/**
 * account admin
 */
router
	.route('/account')
	.get(asyncHandleErr(AccountCtrl.getListAdmin))
	.post(
		[
			[
				check('email')
					.isLength({ min: 4, max: 30 })
					.withMessage('Email tối thiểu 4 ký tự & tối đa 30 ký tự')
					.isEmail()
					.withMessage('Email không hợp lệ'),
				check('password')
					.isLength({ min: 5, max: 30 })
					.withMessage('Mật khẩu tối thiểu 5 ký tự & tối đa 30 ký tự'),
			],
		],
		asyncHandleErr(AccountCtrl.createAdmin),
	);
router
	.route('/account/:ID')
	.patch(asyncHandleErr(AccountCtrl.updateAdmin))
	.delete(asyncHandleErr(AccountCtrl.deleteAdmin));

router.patch(
	'/account/change-password/:ID',
	[
		[
			check('password')
				.not()
				.isEmpty()
				.withMessage('Không được để trống mật khẩu'),
			check('newPassword')
				.not()
				.isEmpty()
				.withMessage('Không được để trống mật khẩu mới')
				.exists()
				.custom((value, { req }) => value === req.body.password)
				.withMessage('Không được nhập giống mật khẩu cũ'),
			check('confirmNewPassword')
				.not()
				.isEmpty()
				.withMessage('Không được để trống xác thực mật khẩu mới')
				.exists()
				.custom((value, { req }) => value === req.body.password)
				.withMessage('Giá trị input nhập lại mật khẩu không giống giá trị input mật khẩu'),
		],
	],
	asyncHandleErr(AccountCtrl.changePasswordAdmin),
);

/**
 * account student
 */
router
	.route('/account/student')
	.get(asyncHandleErr(AccountCtrl.getListStudent))
	.post(
		[
			[
				check('email')
					.isLength({ min: 4, max: 30 })
					.withMessage('Email tối thiểu 4 ký tự & tối đa 30 ký tự')
					.isEmail()
					.withMessage('Email không hợp lệ'),
				check('password')
					.isLength({ min: 5, max: 30 })
					.withMessage('Mật khẩu tối thiểu 5 ký tự & tối đa 30 ký tự'),
				check('name')
					.isLength({ min: 5, max: 30 })
					.withMessage('Tối thiểu 5 ký tự & tối đa 30 ký tự'),
				check('country')
					.not()
					.isEmpty()
					.withMessage('Không được để trống mục địa điểm'),
				check('mobilePhone')
					.not()
					.isEmpty()
					.withMessage('Không được để trống số điện thoại')
					.isLength({ min: 11, max: 11 })
					.withMessage('Số điện thoại không hợp lệ'),
				check('birthday')
					.not()
					.isEmpty()
					.withMessage('Không được để trống ngày sinh'),
			],
		],
		asyncHandleErr(AccountCtrl.createStudent),
	);

router.get('/account/student/search', asyncHandleErr(AccountCtrl.searchStudent));

router
	.route('/account/student/:ID')
	.patch(asyncHandleErr(AccountCtrl.updateStudent))
	.delete(asyncHandleErr(AccountCtrl.deleteStudent));

router.patch(
	'/account/student/change-password/:ID',
	[
		[
			check('password')
				.not()
				.isEmpty()
				.withMessage('Không được để trống mật khẩu'),
			check('newPassword')
				.not()
				.isEmpty()
				.withMessage('Không được để trống mật khẩu mới')
				.exists()
				.custom((value, { req }) => value === req.body.password)
				.withMessage('Không được nhập giống mật khẩu cũ'),
			check('confirmNewPassword')
				.not()
				.isEmpty()
				.withMessage('Không được để trống xác thực mật khẩu mới')
				.exists()
				.custom((value, { req }) => value === req.body.password)
				.withMessage('Giá trị input nhập lại mật khẩu không giống giá trị input mật khẩu'),
		],
	],
	asyncHandleErr(AccountCtrl.changePasswordStudent),
);
/**
 * account teacher
 */
router
	.route('/account/teacher')
	.get(asyncHandleErr(AccountCtrl.getListTeacher))
	.post(
		[
			[
				check('email')
					.isLength({ min: 4, max: 30 })
					.withMessage('Email tối thiểu 4 ký tự & tối đa 30 ký tự')
					.isEmail()
					.withMessage('Email không hợp lệ'),
				check('password')
					.isLength({ min: 5, max: 30 })
					.withMessage('Mật khẩu tối thiểu 5 ký tự & tối đa 50 ký tự'),
				check('name')
					.isLength({ min: 5, max: 30 })
					.withMessage('Tối thiểu 5 ký tự & tối đa 30 ký tự'),
				check('country')
					.not()
					.isEmpty()
					.withMessage('Không được để trống mục địa điểm'),
				check('mobilePhone')
					.not()
					.isEmpty()
					.withMessage('Không được để trống số điện thoại')
					.isLength({ min: 11, max: 11 })
					.withMessage('Số điện thoại không hợp lệ'),
				check('birthday')
					.not()
					.isEmpty()
					.withMessage('Không được để trống ngày sinh'),
			],
		],
		asyncHandleErr(AccountCtrl.createTeacher),
	);
router.get('/account/teacher/search',AccountCtrl.searchTeacher)
router
	.route('/account/teacher/:ID')
	.patch(asyncHandleErr(AccountCtrl.updateTeacher))
	.delete(asyncHandleErr(AccountCtrl.deleteTeacher));

router.patch(
	'/account/teacher/change-password/:ID',
	[
		[
			check('password')
				.not()
				.isEmpty()
				.withMessage('Không được để trống mật khẩu'),
			check('newPassword')
				.not()
				.isEmpty()
				.withMessage('Không được để trống mật khẩu mới')
				.exists()
				.custom((value, { req }) => value === req.body.password)
				.withMessage('Không được nhập giống mật khẩu cũ'),
			check('confirmNewPassword')
				.not()
				.isEmpty()
				.withMessage('Không được để trống xác thực mật khẩu mới')
				.exists()
				.custom((value, { req }) => value === req.body.password)
				.withMessage('Giá trị input nhập lại mật khẩu không giống giá trị input mật khẩu'),
		],
	],
	asyncHandleErr(AccountCtrl.changePasswordTeacher),
);

export default router;
