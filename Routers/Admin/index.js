import app from 'express';
import { middlewareAdmin } from '../../Utils/authRouter';

import {asyncHandleErr} from '../../Utils/cacthError';
import AccountCtrl from '../../Controllers/Admin/Account.ctrl';
import AccountRoute from './Account.route';

const router = app.Router();

/**
 * middleware router
 */
router.use(middlewareAdmin);

router.use(AccountRoute);

router.get('/get-profile',	asyncHandleErr(AccountCtrl.getProfileAdmin))

export default router;
